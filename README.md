# SE 101 Braille Printer

We created a braille printing machine that proves we can make printing more accessible for the blind and visually impaired. The cost of the printer is 6x less than many commercial options on the market!

Created by: Abhishek Parapuram, Eric Xu, Ilan Benjamin, Jimmy Huynh,
Max Ning, Tanbir Singh Banipal

This project was a collaborative effort between 6 people. We engineered and programmed the device completely remotely! 

To learn more about this project, check these out:

[Video](https://www.youtube.com/watch?v=qBbNj5UoF6o)

[Full Report](https://docs.google.com/document/d/1un--zDu14lB02-deoiP9gMgdpVrWfaafFBJ3uR9PBFo/edit?usp=sharing)
